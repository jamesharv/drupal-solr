#!/bin/bash
set -xe

if [ ! -d "/mnt/solr" ]; then
  cp -r /opt/solr/example/multicore /mnt/solr
  rm -rf /mnt/solr/apachesolr
  rm -rf /mnt/solr/search_api
fi

if [ ! -d "/var/log/solr" ]; then
  mkdir -p /var/log/solr
fi

for core in "`env | grep CORE_ | cut -d = -f 2-`";
do
  if [ -n "$core" ]; then
    NAME=`echo $core | cut -d : -f 1`
    TYPE=`echo $core | cut -d : -f 2`
    CONF_PATH=`echo $core | cut -d : -f 3`

    if [ ! -d "/mnt/solr/$NAME" ]; then
      cp -r /opt/solr/example/multicore/$TYPE /mnt/solr/$NAME
      sed -i "s@<!-- core -->@<core name=\"$NAME\" instanceDir=\"$NAME\" /><!-- core -->@g" /mnt/solr/solr.xml
    fi

    # If $CONF_PATH provided, use config from that directory.
    if [ -n "$CONF_PATH" ]; then
      rm -rf /mnt/solr/$NAME/conf
      ln -s $CONF_PATH /mnt/solr/$NAME/conf
    fi
  fi
done

java -jar /opt/solr/example/start.jar -Dsolr.contrib.dir=/opt/solr/contrib -Dsolr.solr.home=/mnt/solr 2>> /var/log/solr/error.log 1>> /var/log/solr/solr.log
